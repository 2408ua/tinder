package api.models;

public class User {
  int id;
  int likedId;
  String email;
  String name;
  String password;
  String photoLink;
  String profession;
  String likedStatus;

  public int getId() {
    return id;
  }

  public int getLikedId() {
    return likedId;
  }

  public String getLikedStatus() {
    return likedStatus;
  }

  public String getStringId() {
    return String.valueOf(id);
  }

  public String getEmail() {
    return email;
  }

  public String getName() {
    return name;
  }

  public String getFirstName() {
    return name.split(" ")[0];
  }

  public String getSurName() {
    return name.split(" ")[1];
  }

  public String getPassword() {
    return password;
  }

  public String getPhotoLink() {
    return photoLink;
  }

  public String getProfession() {
    return profession;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setLikedId(int likedId) {
    this.likedId = likedId;
  }

  public void setLikedStatus(String likedStatus) {
    this.likedStatus = likedStatus;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setPhotoLink(String photoLink) {
    this.photoLink = photoLink;
  }

  public void setProfession(String profession) {
    this.profession = profession;
  }

  public boolean isIdZero(){
    return this.id == 0;
  }
}
