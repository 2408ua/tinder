package api.models;

public class Message {
  private final int userId;
  private final int chatUserId;
  private final String text;
  private final String createdAt;

  public Message(int userId, int chatUserId, String text, String createdAt) {
    this.userId = userId;
    this.chatUserId = chatUserId;
    this.text = text;
    this.createdAt = createdAt;
  }

  public int getUserId() {
    return userId;
  }

  public int getChatUserId() {
    return chatUserId;
  }

  public String getText() {
    return text;
  }

  public String getCreatedAt() {
    return createdAt;
  }
}
