package api.dao;

import api.models.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDao extends DbConnect {
  Statement statement;

  public UserDao() throws SQLException {
    super();
    this.statement = this.getStatement();
  }

  private User userMapping(String query) throws SQLException {
    ResultSet rs = statement.executeQuery(query);
    User user = new User();
    while (rs.next()) {
      user.setId(rs.getInt("id"));
      user.setEmail(rs.getString("email"));
      user.setName(rs.getString("name"));
      user.setPassword(rs.getString("password"));
      user.setPhotoLink(rs.getString("photo_link"));
    }
    return user;
  }

  public User getUserById(int id) throws SQLException {
    String query = Queries.getUserById(id);
    return userMapping(query);
  }

  public User getUserByEmailAndPassword(String email, String password) throws SQLException {
    String query = Queries.getUserByEmailAndPassword(email, password);
    return userMapping(query);
  }

  public User getNotLikedUser(int userId) throws SQLException {
    String query = Queries.getNotLikedUser(userId);
    return userMapping(query);
  }

  public List<User> getLikedUsers(int userId, int status) throws SQLException {
    String query = Queries.getUsersByStatus(userId, status);
    ResultSet rs = statement.executeQuery(query);
    List<User> users = new ArrayList<>();

    while (rs.next()) {
      User user = new User();
      user.setId(rs.getInt("userId"));
      user.setName(rs.getString("name"));
      user.setPhotoLink(rs.getString("photoLink"));
      user.setLikedId(rs.getInt("likedId"));
      user.setProfession(rs.getString("profession"));
      users.add(user);
    }
    return users;
  }
}
