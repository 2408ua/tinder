package api.dao;

import api.models.Message;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MessagesDao extends DbConnect {
  Statement statement;

  public MessagesDao() throws SQLException {
    super();
    this.statement = this.getStatement();
  }

  public List<Message> getMessages(int userId, int chatUserId) throws SQLException {
    String query = Queries.getMessages(userId, chatUserId);
    ResultSet rs = statement.executeQuery(query);
    List<Message> messages = new ArrayList<>();

    while (rs.next()) {
      Message message = new Message(
          rs.getInt("userId"),
          rs.getInt("chatUserId"),
          rs.getString("text"),
          rs.getString("createdAt")
      );
      messages.add(message);
    }
    return messages;
  }

  public void setMessage(int userId, int chatUserId, String text) throws SQLException {
    String query = Queries.setMessage(userId, chatUserId, text);
    statement.executeUpdate(query);
  }
}
