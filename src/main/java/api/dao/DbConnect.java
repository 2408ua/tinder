package api.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DbConnect {
  Connection conn;

  public DbConnect() throws SQLException {
    String dockerURL = "jdbc:postgresql://172.19.0.2/tinder?user=postgres&password=root";
    String herokuURL = "jdbc:postgresql://ec2-54-73-152-36.eu-west-1.compute.amazonaws.com:5432/df7i2p5m3rhbnm?user=cqgjhysodvkbzs&password=9ddcc5e3a961468d38d1281c50a13308479d23b1e3dd0f5fb6ac5dc825c9cbd0";
    this.conn = DriverManager.getConnection(herokuURL);
  }

  public Statement getStatement() throws SQLException {
    return conn.createStatement();
  }

  public void connectionClose() throws SQLException {
    conn.close();
  }
}
