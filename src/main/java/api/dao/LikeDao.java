package api.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LikeDao extends DbConnect {
  Statement statement;

  public LikeDao() throws SQLException {
    super();
    this.statement = this.getStatement();
  }

  public void setStatus(int userId, int userLikedId, String status) throws SQLException {
    String query = Queries.setStatus(userId, userLikedId, status);
    statement.executeUpdate(query);
  }
}
