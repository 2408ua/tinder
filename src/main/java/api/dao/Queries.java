package api.dao;

public class Queries {

  public static String getUserByEmailAndPassword(String email, String password) {
    return "SELECT * FROM users " +
        "WHERE email = " +
        "\'" + email + "\'" +
        "AND password = " +
        "\'" + password + "\'" + ";";
  }

  public static String getUserById(int id) {
    return "SELECT * FROM users where id = " + id + ";";
  }

  public static String getNotLikedUser(int userId) {
    return "select * from users " +
        "where users.id not in (Select users.id " +
        "from users JOIN liked l on users.id = l.user_liked_id " +
        "where l.user_id = " + userId + " and l.status <> 0) " +
        "and users.id <> " + userId +
        " LIMIT 1;";
  }

  public static String getUsersByStatus(int userId, int status) {
    return "Select u.id as userId, u.name as name," +
        " u.profession as profession, " +
        " u.photo_link as photoLink, l.id as likedId " +
        "from users u JOIN liked l on u.id = l.user_liked_id " +
        "where l.user_id = " + userId + " and l.status = " + status + ";";
  }

  public static String setStatus(int userId, int userLikedId, String status) {
    return "insert into liked (user_id, user_liked_id, status) values (" +
        userId + ", " + userLikedId + ", " + status + ");";
  }

  public static String getMessages(int userId, int chatUserId) {
    return "select m.user_id as userId, m.chat_user_id as chatUserId, m.text as text, m.\"createdAt\" as createdAt" +
        " from messages m " +
        "join users u on u.id = m.user_id " +
        "where (user_id = " + userId +
        " and chat_user_id = " + chatUserId + ") " +
        "or (user_id = " + chatUserId +
        " and chat_user_id = " + userId + ") " +
        "order by createdAt;";
  }

  public static String setMessage(int userId, int chatUserId, String text) {
    return "insert into messages (user_id, chat_user_id, text) values (" +
        userId + ", " + chatUserId + ", " + "\'" + text.replaceAll("'", "''") + "\'" + ");";
  }
}
