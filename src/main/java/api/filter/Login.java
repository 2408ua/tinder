package api.filter;

import api.services.CookiesService;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.*;
import java.io.IOException;

@WebFilter("/*")
public class Login implements Filter {
  public void init(FilterConfig config) throws ServletException {
  }

  public void destroy() {
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;
    Cookie loginCookie = CookiesService.getCookieByName(req, res, "name");
    String requestUrl = req.getRequestURI();

    if (loginCookie == null &&
        (requestUrl.equals("/api/users") ||
            requestUrl.equals("/api/") ||
            requestUrl.equals("/api/messages") ||
            requestUrl.equals("/api/liked-list")
        )) {
      res.sendRedirect("login");

    } else {
      chain.doFilter(request, response);
    }
  }
}
