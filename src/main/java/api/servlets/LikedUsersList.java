package api.servlets;

import api.models.User;
import api.services.CookiesService;
import api.services.LikeService;
import api.services.UserService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "api.servlets.LikedUsersList", value = "/liked-list")
public class LikedUsersList extends HttpServlet {
  private final int LIKE_STATUS = 1;
  private final String ID_COOKIE_NAME = "id";
  UserService userService;

  public LikedUsersList() throws SQLException {
    this.userService = new UserService();
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      Cookie idCookie = CookiesService.getCookieByName(request, response, ID_COOKIE_NAME);
      int userId = Integer.parseInt(idCookie.getValue());

      List<User> users = this.userService.getLikedUsers(userId, LIKE_STATUS);

      response.setContentType("text/html");
      request.setAttribute("users", users);
      String path = "/views/people_list.jsp";
      ServletContext servletContext = getServletContext();
      RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(path);
      requestDispatcher.forward(request, response);

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String id = request.getParameter("chatUserId");
    response.sendRedirect("messages?id=" + id);
  }
}
