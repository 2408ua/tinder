package api.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

import api.services.CookiesService;

@WebServlet(name = "Logout", value = "/logout")
public class Logout extends HttpServlet {

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    Cookie loginCookie = CookiesService.getCookieByName(request, response, "name");
    Cookie idCookie = CookiesService.getCookieByName(request, response, "id");
    if (loginCookie != null) {
      loginCookie.setMaxAge(0);
      response.addCookie(loginCookie);
    }

    if (idCookie != null) {
      idCookie.setMaxAge(0);
      response.addCookie(idCookie);
    }
    response.sendRedirect("login");
  }
}
