package api.servlets;

import api.models.User;
import api.services.CookiesService;
import api.services.LikeService;
import api.services.UserService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "api.servlets.Users", value = "/users")
public class Users extends HttpServlet {
  UserService userService;
  LikeService likeService;

  public Users() throws SQLException {
    this.userService = new UserService();
    this.likeService = new LikeService();
  }

  private void getNoLikedUserPage(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
    Cookie idCookie = CookiesService.getCookieByName(request, response, "id");
    int userId = Integer.parseInt(idCookie.getValue());
    User user = this.userService.getNotLikedUser(userId);

    if (user.isIdZero()) {
      response.sendRedirect("liked-list");
      return;
    }

    response.setContentType("text/html");
    request.setAttribute("name", user.getName());
    request.setAttribute("userId", userId);
    request.setAttribute("userLikedId", user.getId());
    request.setAttribute("photoLink", user.getPhotoLink());

    String path = "/views/user.jsp";
    ServletContext servletContext = getServletContext();
    RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(path);
    requestDispatcher.forward(request, response);
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      getNoLikedUserPage(request, response);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      response.setContentType("text/html");
      int userId = Integer.parseInt(request.getParameter("userId"));
      int userLikedId = Integer.parseInt(request.getParameter("userLikedId"));
      String status = request.getParameter("status");

      this.likeService.setStatus(userId, userLikedId, status);

      getNoLikedUserPage(request, response);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
