package api.servlets;

import java.sql.*;

public class Test {
  public static void main(String[] args) throws SQLException {
    try {
      String url = "jdbc:postgresql://localhost:5432/tinder?user=postgres&password=root";
      Connection conn = DriverManager.getConnection(url);
      System.out.println("Success connect to DB");
      Statement statement = conn.createStatement();
      ResultSet rs = statement.executeQuery("SELECT * FROM users");
      while ( rs.next() ) {
        int id = rs.getInt("id");
        String  name = rs.getString("name");
        System.out.println( "ID = " + id );
        System.out.println( "NAME = " + name );
        System.out.println();
      }
      conn.close();

    } catch (SQLException e) {
      System.out.println("Error connect to DB");
      e.printStackTrace();
    }

  }

}
