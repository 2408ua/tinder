package api.servlets;

import api.models.User;
import api.services.UserService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "api.servlets.Login", value = "/login")
public class Login extends HttpServlet {
  UserService userService;

  public Login() throws SQLException {
    this.userService = new UserService();
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    String path = "/views/login.jsp";
    request.setAttribute("errorMessage", "");
    request.setAttribute("email", "");
    request.setAttribute("password", "");
    ServletContext servletContext = getServletContext();
    RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(path);
    requestDispatcher.forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    String email = request.getParameter("email");
    String password = request.getParameter("password");

    try {
      User user = userService.getUserByEmailAndPassword(email, password);

      if (user.isIdZero()) {
        request.setAttribute("errorMessage", "User or Password is not correct");
        request.setAttribute("email", email);
        request.setAttribute("password", password);
        getServletContext().getRequestDispatcher("/views/login.jsp").forward(request, response);

      } else {
        Cookie loginCookie = new Cookie("name", user.getFirstName());
        Cookie idCookie = new Cookie("id", user.getStringId());
        int lifeTime = 30 * 60;
        loginCookie.setMaxAge(lifeTime);
        response.addCookie(loginCookie);
        idCookie.setMaxAge(lifeTime);
        response.addCookie(idCookie);

        response.sendRedirect("users");
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
