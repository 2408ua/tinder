package api.servlets;

import api.models.Message;
import api.models.User;
import api.services.CookiesService;
import api.services.MessageService;
import api.services.UserService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "api.servlets.Chat", value = "/messages")
public class Chat extends HttpServlet {
  MessageService messageService;
  UserService userService;

  public Chat() throws SQLException {
    this.messageService = new MessageService();
    this.userService = new UserService();
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      Cookie idCookie = CookiesService.getCookieByName(request, response, "id");
      int userId = Integer.parseInt(idCookie.getValue());
      int chatUserId = Integer.parseInt(request.getParameter("id"));
      User chatUser = this.userService.getUserById(chatUserId);
      List<Message> messages = this.messageService.getMessages(userId, chatUserId);

      response.setContentType("text/html");
      request.setAttribute("messages", messages);
      request.setAttribute("chatUser", chatUser);
      request.setAttribute("userId", userId);
      String path = "/views/chat.jsp";
      ServletContext servletContext = getServletContext();
      RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(path);
      requestDispatcher.forward(request, response);

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      Cookie idCookie = CookiesService.getCookieByName(request, response, "id");
      int userId = Integer.parseInt(idCookie.getValue());
      String newText = request.getParameter("text");
      int chatUserId = Integer.parseInt(request.getParameter("id"));
      this.messageService.setMessage(userId, chatUserId, newText);
      User chatUser = this.userService.getUserById(chatUserId);
      List<Message> messages = this.messageService.getMessages(userId, chatUserId);

      response.setContentType("text/html");
      request.setAttribute("messages", messages);
      request.setAttribute("chatUser", chatUser);
      request.setAttribute("userId", userId);
      String path = "/views/chat.jsp";
      ServletContext servletContext = getServletContext();
      RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(path);
      requestDispatcher.forward(request, response);

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
