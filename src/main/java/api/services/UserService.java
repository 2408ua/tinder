package api.services;

import api.dao.Queries;
import api.dao.UserDao;
import api.models.User;

import java.sql.SQLException;
import java.util.List;

public class UserService {
  UserDao userDao;

  public UserService() throws SQLException {
    this.userDao = new UserDao();
  }

  public boolean isUserExist(String email, String password) throws SQLException {
    User user = userDao.getUserByEmailAndPassword(email, password);

    return !user.isIdZero();
  }

  public User getUserById(int id) throws SQLException {
    return userDao.getUserById(id);
  }

  public User getUserByEmailAndPassword(String email, String password) throws SQLException {
    return userDao.getUserByEmailAndPassword(email, password);
  }

  public User getNotLikedUser(int userId) throws SQLException {
    return userDao.getNotLikedUser(userId);
  }

  public List<User> getLikedUsers(int userId, int status) throws SQLException {
    return userDao.getLikedUsers(userId, status);
  }


}
