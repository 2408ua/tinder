package api.services;

import api.dao.LikeDao;

import java.sql.SQLException;

public class LikeService {
  LikeDao likeDao;

  public LikeService() throws SQLException {
    this.likeDao = new LikeDao();
  }

  public void setStatus(int userId, int userLikedId, String status) throws SQLException {
    likeDao.setStatus(userId, userLikedId, status);
  }
}
