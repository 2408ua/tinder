package api.services;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookiesService {

  public static Cookie getCookieByName(HttpServletRequest request, HttpServletResponse response, String name) {
    Cookie loginCookie = null;
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().equals(name)) {
          loginCookie = cookie;
          break;
        }
      }
    }
    return loginCookie;
  }

}
