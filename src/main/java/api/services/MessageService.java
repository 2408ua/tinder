package api.services;

import api.dao.MessagesDao;
import api.models.Message;
import java.sql.SQLException;
import java.util.List;

public class MessageService {
  MessagesDao messageDao;

  public MessageService() throws SQLException {
    this.messageDao = new MessagesDao();
  }

  public List<Message> getMessages(int userId, int chatUserId) throws SQLException {
    return this.messageDao.getMessages(userId, chatUserId);
  }

  public void setMessage(int userId, int chatUserId, String text) throws SQLException {
    this.messageDao.setMessage(userId, chatUserId, text);
  }
}
