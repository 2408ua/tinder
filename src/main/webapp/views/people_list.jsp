<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/favicon.ico">

    <title>People list</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<jsp:include page="/views/header.jsp"></jsp:include>
<div class="container">
    <div class="row">
        <div class="col-8 offset-2">
            <div class="panel panel-default user_panel">
                <div class="panel-heading">
                    <h3 class="panel-title">User List</h3>
                </div>
                <div class="panel-body">
                    <div class="table-container">
                        <form id="liked_post" action="/api/liked-list" method="post">
                            <table class="table-users table" border="0">
                                <tbody>
                                <input type="hidden" name="chatUserId" id="chatUserId" value="0"/>
                                <c:forEach items="${users}" var="user">
                                    <tr onclick="goToChat(${user.getId()})">
                                        <td width="10">
                                            <div class="avatar-img">
                                                <img class="img-circle" src="${user.getPhotoLink()}"/>
                                            </div>
                                        </td>
                                        <td class="align-middle">${user.getName()}</td>
                                        <td class="align-middle profession">${user.getProfession()}</td>
<%--                                        <td class="align-middle">--%>
<%--                                            Last api.servlets.Login: 6/10/2017<br>--%>
<%--                                            <small class="text-muted">5 days ago</small>--%>
<%--                                        </td>--%>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<jsp:include page="/views/script.jsp"></jsp:include>
</html>

