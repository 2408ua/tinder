<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/favicon.ico">

    <title>api.servlets.Like page</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="css/style.css">
</head>
<body style="background-color: #f5f5f5;">
<jsp:include page="/views/header.jsp"></jsp:include>
<div class="main">
    <div class="col-4 offset-4">
        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-center disabled-spinner">
                    <img src="img/Spinner-5.gif" alt="loading...">
                </div>
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12 text-center">
                        <img src="${photoLink}" alt=""
                             class="mx-auto rounded-circle img-fluid">
                        <h3 class="mb-0 text-truncated">${name}</h3>
                        <br>
                    </div>
                    <div class="col-12 col-lg-6">
                        <form action="/api/users" method="post">
                            <input type="hidden" name="userId" value="${userId}">
                            <input type="hidden" name="userLikedId" value="${userLikedId}">
                            <input type="hidden" name="status" value="2">
                            <button onclick="sendDislike()" type="submit" class="btn btn-outline-danger btn-block"><span
                                    class="fa fa-times"></span>
                                Dislike
                            </button>
                        </form>
                    </div>
                    <div class="col-12 col-lg-6">
                        <form action="/api/users" method="post">
                            <input type="hidden" name="userId" value="${userId}">
                            <input type="hidden" name="userLikedId" value="${userLikedId}">
                            <input type="hidden" name="status" value="1">
                            <button onclick="sendLike()" class="btn btn-outline-success btn-block"><span
                                    class="fa fa-heart"></span>
                                Like
                            </button>
                        </form>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
            <!--/card-block-->
        </div>
    </div>
</div>
</body>
</html>
