<%
    String name = null;
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("name")) name = cookie.getValue();
        }
    }
    pageContext.setAttribute("name", name);
%>
<header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between mb-4 border-bottom">
    <div class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
        <img src="https://icons.getbootstrap.com/assets/img/bootstrap.svg" alt="" width="52" height="72">
    </div>

    <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
        <li><a href="users" class="nav-link px-2 link-dark">Users</a></li>
        <li><a href="liked-list" class="nav-link px-2 link-secondary">Liked</a></li>
<%--        <li><a href="disliked-list" class="nav-link px-2 link-secondary">Disliked</a></li>--%>
    </ul>

    <div class="col-md-3 text-end d-flex flex-md-row align-items-center">
        <p class="welcome">Hello, ${name}!</p>
        <form class="logout-form" action="/api/logout" method="post">
            <button type="submit" class="btn btn-outline-light">Sign-out</button>
        </form>
    </div>
</header>
